#include "REDIS_API.h"
#include <fstream>
#include <cstdio>
#include <string>
#include <iostream>
#include <utility>
#include <vector>

void REDIS_API::swapData() {
    std::fstream inFile;
    inFile.open(SWAP_FILE, std::fstream::in);
    if(inFile.eof()) {
        inFile.close();
        return;
    } else {
        std::system("cp swap.txt data.txt");
        std::system("rm log.txt");
    }
}


void REDIS_API::readKeyValueData() {
    std::fstream file;
    file.open(DATA_FILE, std::fstream::in);
    std::string key,
                value;
    while (file >> key >> value) {
        dataKeyValue[key] = value;
    }
    file.close();
}


void REDIS_API::readKeyValueLog() {
    std::fstream file;
    file.open(LOG_FILE, std::fstream::in);
    std::string key,
                value;
    while (file >> key >> value) {
        logKeyValue[key] = value;
    }
    file.close();
}


void REDIS_API::applyChanges() {
    for (auto it = logKeyValue.begin(); it != logKeyValue.end(); ++it) {
        dataKeyValue[it->first] = it->second;
    }
    logKeyValue.clear();
}


void REDIS_API::writeKeyValueData() {
    std::fstream file;
    file.open(SWAP_FILE, std::fstream::out | std::fstream::trunc);
    for (auto it = dataKeyValue.begin(); it != dataKeyValue.end(); ++it) {
        file << it->first
             << " "
             << it->second
             << std::endl;
    }
    file.close();
    swapData();
}


void REDIS_API::writeKeyValuelog() {
    std::fstream file;
    if (logKeyValue.size() == 0) return;
    file.open(LOG_FILE, std::fstream::out | std::fstream::app);
    for (auto it = logKeyValue.begin(); it != logKeyValue.end(); ++it) {
        file << it->first
        << " "
        << it->second
        << std::endl;
    }
    logKeyValue.clear();
    file.close();
}


void REDIS_API::checkFileExist() {
    if (!std::ifstream(DATA_FILE)) {
        std::ofstream file(DATA_FILE);
    }
    if (!std::ifstream(SWAP_FILE)) {
        std::ofstream file(SWAP_FILE);
    }
    if (!std::ifstream(LOG_FILE)) {
        std::ofstream file(LOG_FILE);
    }

}

std::string REDIS_API::getStr(int& st,std::string& str) {
    std::string res = "";
    int num;
    const char *buf = str.c_str();
    sscanf(buf + st, "$%i", &num);
    auto it = buf + st;
    
    while (*it++ != '\r') {}
    ++it;
    for (int i = 0; i < num; ++i) {
        res += *it++;
    }
    
    it += 2;
    st = int(it - buf);
    return res;
}

bool REDIS_API::getNaiveRepr(std::string& str, struct OpWithArg& res) {
    int num;
    const char *buf = str.c_str();
    sscanf(buf, "*%i\r\n", &num);
    if (num < 2 || num > 3) {
        return 0;
    }
    
    int st = 0;
    while (*(buf + st) != '\r')
        ++st;
    st += 2;
    res.op = getStr(st, str);
    res.key = getStr(st, str);
    if (num == 3) res.value = getStr(st, str);
    
    return 1;
}



REDIS_API::REDIS_API() {
    checkFileExist();
    readKeyValueLog();
    readKeyValueData();
    applyChanges();
    writeKeyValueData();
    checkFileExist();
}


std::string REDIS_API::getValue(std::string& key) {
    std::string value;
    if (dataKeyValue.find(key) != dataKeyValue.end()) {
        value = dataKeyValue[key];
    } else {
        value = "NO KEY IN DATA";
    }
    return value;
}


void REDIS_API::setKeyValue(std::string& key, std::string& value) {
    dataKeyValue[key] = value;
    logKeyValue[key] = value;
    writeKeyValuelog();
}

