#ifndef receive_source_hpp
#define receive_source_hpp

#include <stdio.h>
#include <deque>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <stdlib.h>
#include <sys/time.h>

enum State {
    start, finish, array_size, fault,
    string_size, array_body, string_body
};

class Client {
private:
    int num_req = 0;
    State state = start;
    std::deque<char> buf;
    std::string current_str = "";
    long long arr_size = 0;
    long long str_size = 0;
public:
    std::vector<std::vector<std::string>> request;
    int receive_request(int socket);
    size_t size();
    int recv_all(int socket);
    long long getNumReq();
    void changeNumReq();
    int automaton();
    void clear();
    State getState();
};


#endif /* receive_source_hpp */
