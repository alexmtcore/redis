#include <map>
#include <string>

const char DATA_FILE[9] = "data.txt";
const char LOG_FILE[8] = "log.txt";
const char SWAP_FILE[14] = "swap.txt";

struct OpWithArg {
    std::string op, key, value;
};


class REDIS_API {
private:
    std::map<std::string, std::string> dataKeyValue,
                                       logKeyValue;
public:
    REDIS_API();
    std::string getValue(std::string&);
    void setKeyValue(std::string&, std::string&);
    void changeLog(std::string&, std::string&);
    void swapData();
    void applyChanges();
    void readKeyValueData();
    void readKeyValueLog();
    void writeKeyValueData();
    void writeKeyValuelog();
    void checkFileExist();
    bool getNaiveRepr(std::string&, struct OpWithArg&);
    std::string getStr(int&, std::string&);
};
