#include "receive_source.hpp"
#include "REDIS_API.h"

const long MAX_REQ_SIZE = 1024;

size_t Client::size() {
    return request.size();
}

State Client::getState() {
    return state;
}

long long Client::getNumReq() {
    return num_req;
}

void Client::changeNumReq() {
    num_req = 0;
}

void Client::clear() {
    buf.clear();
    request.clear();
    arr_size = 0;
    str_size = 0;
    num_req = 0;
    state = start;
    current_str = "";
}



int Client::recv_all(int socket) {
    long bytes = 1;
    
    while (bytes > 0) {
        char buffer[MAX_REQ_SIZE];
        bytes = recv(socket, buffer, MAX_REQ_SIZE, 0);
        if (bytes <= 0) break;
        for (size_t i = 0; i < bytes; ++i) {
            buf.push_back(buffer[i]);
        }
    }
    
    if (buf.empty()) return 0;
    
    return 1;
}

int Client::receive_request(int socket) {
    if (!recv_all(socket)) return 0;
    int res = automaton();
    
    return res;
}

int Client::automaton() {
    
    
    for (int i = 0; i < buf.size();) {
        char symbol = buf[i];
        
        if (state != finish) ++i;
        
        switch (state) {
            case start:
            {
                if (symbol == '*') {
                state = array_size;
                break;
                }
                
                state = fault;
                break;
            }
                    
            case array_size:
            {
                if (('0' <= symbol) && (symbol <= '9')) {
                    arr_size = 10 * arr_size + (symbol - '0');
                    break;
                }
            
                if (symbol == '\r') {
                    break;
                }
            
                if (symbol == '\n') {
                    if (arr_size == 0) {
                        state = fault;
                        break;
                    }
            
                    state = array_body;
                    break;
                }
                
                state = fault;
                break;
            }
                
            case array_body:
            {
                if (symbol == '$') {
                    state = string_size;
                    break;
                }
                
                state = fault;
                break;
            }
            
            case string_size:
            {
                if (('0' <= symbol) && (symbol <= '9')) {
                    str_size = 10 * str_size + (symbol - '0');
                    break;
                }
                
                if (symbol == '\r') {
                    break;
                }
                    
                if (symbol == '\n') {
                    state = string_body;
                    break;
                }
                    
                state = fault;
                break;
            }

            case string_body:
            {
                if ((symbol != '\r') && (symbol != '\n')) {
                    current_str += symbol;
                    --str_size;
                    break;
                }
                    
                if (symbol == '\r' && str_size == 0) {
                    break;
                }
                    
                if (symbol == '\n' && str_size == 0) {
                    --arr_size;
                    
                    request.resize(num_req + 1);
                    request[num_req].push_back(current_str);
                    current_str = "";
                    if (arr_size == 0) {
                        state = finish;
                        break;
                    }
                        
                    state = array_body;
                    break;
                }
                                        
                state = fault;
                break;
            }

            case finish:
            {
                arr_size = 0;
                str_size = 0;
                ++num_req;
                state = start;
                break;
            }
                
                    
            case fault:
            {
                buf.clear();
                current_str = "";
                return 0;
            }
                
            default:
                break;
                
        }
        
    }
    
    if (state == finish) {
        arr_size = 0;
        str_size = 0;
        ++num_req;
        state = start;
    }
    
    if (state == fault) {
        buf.clear();
        current_str = "";
        return 0;
    }
    
    buf.clear();
    return 1;
}
























