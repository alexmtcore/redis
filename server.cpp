#include "server.hpp"
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <algorithm>
#include <vector>
#include <stdlib.h>
#include "REDIS_API.h"
#include <string>

const int PORT = 3425;
const char *error = "Error";
const char *set = "OK";

void sendAll(int socket, const char *buf) {
    long read_bytes = 0;
    while (read_bytes < strlen(buf)) {
        read_bytes += send(socket, buf + read_bytes, strlen(buf) - read_bytes, 0);
    }
}

void server() {
    REDIS_API data;
    int serv_socket;
    struct sockaddr_in address;
    char buf[1024];
    
    serv_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (serv_socket < 0) {
        perror("socket");
        exit(1);
    }
    
    fcntl(serv_socket, F_SETFL, O_NONBLOCK);
    
    address.sin_family = AF_INET;
    address.sin_port = htons(PORT);
    address.sin_addr.s_addr = INADDR_ANY;
    if (bind(serv_socket, (struct sockaddr*)&address, sizeof(address)) < 0) {
        perror("bind");
        exit(2);
    }
    
    listen(serv_socket, 2);
    
    std::vector<int> clients;
        
    while (1) {
        fd_set readset;
        FD_ZERO(&readset);
        FD_SET(serv_socket, &readset);
        
        for (int i = 0; i < clients.size(); ++i) {
            if (clients[i] > 0)
                FD_SET(clients[i], &readset);
        }
        
        int max_socket;
        
        if (clients.begin() == clients.end()) {
            max_socket = serv_socket;
        } else {
            max_socket = std::max(serv_socket, *std::max_element(clients.begin(), clients.end()));
        }
        
        int activity = select(max_socket + 1, &readset, nullptr, nullptr, nullptr);
        if (activity <= 0) {
            perror("select");
            exit(3);
        }
        
        if (FD_ISSET(serv_socket, &readset)) {
            int socket = accept(serv_socket, nullptr, nullptr);
            
            if (socket < 0) {
                perror("accept");
                exit(3);
            }
            
            fcntl(socket, F_SETFL, O_NONBLOCK);
            int flag = 0;
            for (int i = 0; i < clients.size(); ++i) {
                if (clients[i] == 0) {
                    clients[i] = socket;
                    flag = 1;
                    break;
                }
            }
            if (!flag)
                clients.push_back(socket);
        }
        
        for (int i = 0; i < clients.size(); ++i) {
            if (FD_ISSET(clients[i], &readset)) {
                long read_bytes = recv(clients[i], buf, 1024, 0);
                
                if (read_bytes <= 0) {
                    close(clients[i]);
                    clients[i] = 0;
                    continue;
                }
                
                struct OpWithArg command;
                std::string input = buf;
                int res = data.getNaiveRepr(input, command);
                if (!res) {
                    send(clients[i], error, strlen(error), 0);
                    continue;
                }
                if (command.op == "GET") {
                    const char* res = data.getValue(command.key).c_str();
                    sendAll(clients[i], res);
                    continue;
                }
                if (command.op == "SET") {
                    data.setKeyValue(command.key, command.value);
                    send(clients[i], set, strlen(set), 0);
                    continue;
                }
                send(clients[i], error, strlen(error), 0);
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}